# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path

from .views import DashView, HomeView, SettingsView


admin.autodiscover()


urlpatterns = [
    path("admin/", admin.site.urls),
    re_path(r"^", include("login.urls")),
    re_path(r"^$", HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", DashView.as_view(), name="project.dash"),
    re_path(r"^mail/", include("mail.urls")),
    re_path(r"^microsoft/graph/", include("msgraph.urls")),
    re_path(
        r"^settings/$",
        SettingsView.as_view(),
        name="project.settings",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns

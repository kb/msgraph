# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from msgraph.models import MicrosoftGraphError


@pytest.mark.django_db
def test_demo_data():
    call_command("demo_data_msgraph")


@pytest.mark.django_db
def test_microsoft_graph_test(settings):
    settings.MSGRAPH_APPLICATION_ID = "6731de76-14a6-49ae-97bc-6eba6914391e"
    settings.MSGRAPH_CLIENT_SECRET = "my-client-secret"
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    with pytest.raises(MicrosoftGraphError) as e:
        call_command("microsoft_graph_test")
    assert (
        "You do not have administrator authorisation to use "
        "Microsoft Graph.  Click 'Get Consent' in 'Settings'"
    ) in str(e.value)

# -*- encoding: utf-8 -*-
from django.conf import settings
from django.core.management.base import BaseCommand

from msgraph.service import MicrosoftGraph


class Command(BaseCommand):
    help = "Test the Microsoft Graph service"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        microsoft_graph = MicrosoftGraph()
        email = "patrick.kimber@kbuk.onmicrosoft.com"
        microsoft_graph_user = microsoft_graph.user(email)
        self.stdout.write("microsoft_graph_user")
        self.stdout.write(str(microsoft_graph_user))
        microsoft_graph_drive = microsoft_graph.user_drive(microsoft_graph_user)
        self.stdout.write("microsoft_graph_drive")
        self.stdout.write(str(microsoft_graph_drive))
        microsoft_graph_folder = microsoft_graph.folder(
            microsoft_graph_drive, "NMS"
        )
        self.stdout.write("microsoft_graph_folder")
        self.stdout.write(str(microsoft_graph_folder))
        folder_file_list = microsoft_graph.folder_file_list(
            microsoft_graph_drive, microsoft_graph_folder
        )
        self.stdout.write("folder_file_list")
        file_list = {}
        for count, x in enumerate(folder_file_list, 1):
            file_list[x.name] = x
            self.stdout.write("{}. {}".format(count, x.name))
        file_to_download = file_list["Three.docx"]
        result = microsoft_graph._drive_item_pdf(
            microsoft_graph_drive, file_to_download.file_id
        )
        self.stdout.write("_drive_item_pdf")
        self.stdout.write(str(result))
        self.stdout.write("{} - Complete".format(self.help))

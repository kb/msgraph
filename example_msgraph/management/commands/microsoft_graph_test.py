# -*- encoding: utf-8 -*-
import json
import requests

from django.core.management.base import BaseCommand
from http import HTTPStatus

from msgraph.models import MicrosoftGraphSettings


class Command(BaseCommand):
    help = "Test Microsoft Graph"

    def _drives(self, token):
        # /sites/root/drives
        self.stdout.write("Sites - Root")
        response = requests.get(
            "https://graph.microsoft.com/v1.0/sites/root/drives",
            headers=self._headers(token),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            self.stdout.write(json.dumps(data, indent=4))
            # self.stdout.write("Sites - Root: {}".format(data["displayName"]))
            # result = data["id"]
        return None

    def _drive_item_pdf(self, token, item_ids):
        # https://docs.microsoft.com/en-gb/graph/api/driveitem-get-content-format?view=graph-rest-1.0&tabs=cs
        # /drive/items/{item-id}/content?format={format}
        result = None
        self.stdout.write("Drive - Item - PDF")
        # /drives/{drive-id}/root/children
        for item_id in item_ids:
            response = requests.get(
                "https://graph.microsoft.com/v1.0/drive/items/{}/content?format=pdf".format(
                    item_id
                ),
                headers=self._headers(token),
                allow_redirects=True,
            )
            # self.stdout.write(response.status_code)
            print(response.status_code)
            # print(response.text)
            if HTTPStatus.FOUND == response.status_code:
                self.stdout.write(response.text)
                import ipdb

                ipdb.set_trace()
            elif HTTPStatus.OK == response.status_code:
                open("converted-pdf-file.pdf", "wb").write(response.content)
                # self.stdout.write(response.text)
                # data = response.json()
                # self.stdout.write(json.dumps(data, indent=4))
                # value = data["value"]
                # for item in value:
                #    result.append(item["id"])
                #    name = item["name"]
                #    self.stdout.write("Drive - Item: {}".format(name))
                # result = data["id"]
        return result

    def _drive_items(self, token, drive_id):
        # https://docs.microsoft.com/en-us/graph/api/resources/onedrive?view=graph-rest-1.0#commonly-accessed-resources
        result = []
        self.stdout.write("Drive - Items")
        # /drives/{drive-id}/root/children
        response = requests.get(
            "https://graph.microsoft.com/v1.0/drives/{}/root/children".format(
                drive_id
            ),
            headers=self._headers(token),
        )
        # self.stdout.write(response.text)
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            self.stdout.write(json.dumps(data, indent=4))
            value = data["value"]
            for item in value:
                result.append(item["id"])
                name = item["name"]
                self.stdout.write("Drive - Item: {}".format(name))
            # result = data["id"]
        return result

    def _headers(self, token):
        return {"Authorization": "Bearer {}".format(token)}

    def _sites_root(self, token):
        result = None
        self.stdout.write("Sites - Root")
        response = requests.get(
            "https://graph.microsoft.com/v1.0/sites/root",
            headers=self._headers(token),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            # self.stdout.write(json.dumps(data, indent=4))
            self.stdout.write("Sites - Root: {}".format(data["displayName"]))
            result = data["id"]
        return result

    def _site_drive(self, token, site_id):
        result = None
        self.stdout.write("Site - Default Drive")
        response = requests.get(
            "https://graph.microsoft.com/v1.0/sites/{}/drive".format(site_id),
            headers=self._headers(token),
        )
        # self.stdout.write(response.text)
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            self.stdout.write(json.dumps(data, indent=4))
            self.stdout.write("Site - Default Drive: {}".format(data["name"]))
            result = data["id"]
        return result

    def _site_files(self, token, site_id):
        result = None
        self.stdout.write("Site - Files")
        response = requests.get(
            "https://graph.microsoft.com/v1.0/sites/{}/drive/root/delta".format(
                site_id
            ),
            headers=self._headers(token),
        )
        # self.stdout.write(response.text)
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            self.stdout.write(json.dumps(data, indent=4))
            # self.stdout.write("Site - Default Drive: {}".format(data["name"]))
            # result = data["id"]
        return result

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        microsoft_graph_settings = MicrosoftGraphSettings.load()
        token = microsoft_graph_settings.token()
        # self.stdout.write("Token: {}".format(token))
        # self._drives(token)
        site_id = self._sites_root(token)
        # self._site_files(token, site_id)
        drive_id = self._site_drive(token, site_id)
        item_ids = self._drive_items(token, drive_id)
        self._drive_item_pdf(token, item_ids)
        # self.stdout.write(json.dumps(response.json(), indent=4))
        self.stdout.write("{} - Complete".format(self.help))

# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from msgraph.service import MicrosoftGraph


class Command(BaseCommand):
    """Microsoft Graph - Server Side Connector.

    To build a server side connector, we need to add and remove documents from a
    users OneDrive folder:
    https://www.kbsoftware.co.uk/crm/ticket/4623/

    """

    help = "Microsoft Graph - Server Side Connector"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        microsoft_graph = MicrosoftGraph()
        #
        self.stdout.write("List of all users...")
        user_list = microsoft_graph.user_list()
        for count, user in enumerate(user_list, 1):
            self.stdout.write("{}. {}".format(count, user))
        #
        email = "patrick.kimber@kbuk.onmicrosoft.com"
        self.stdout.write("User with email: {}...".format(email))
        microsoft_graph_user = microsoft_graph.user(email)
        self.stdout.write("{}".format(microsoft_graph_user))
        #
        self.stdout.write(
            "Drive for user : {} ({})...".format(
                microsoft_graph_user.email, microsoft_graph_user.user_id
            )
        )
        microsoft_graph_drive = microsoft_graph.user_drive(microsoft_graph_user)
        self.stdout.write("{}".format(microsoft_graph_drive))
        #
        folder_name = "NMS"
        self.stdout.write(
            "Create '{}' folder in drive {}...".format(
                folder_name, microsoft_graph_drive.drive_id
            )
        )
        microsoft_graph.folder_create_if_not_exists(
            microsoft_graph_drive, folder_name
        )
        #
        self.stdout.write(
            "Get details of the '{}' folder in drive {}...".format(
                folder_name, microsoft_graph_drive.drive_id
            )
        )
        microsoft_graph_folder = microsoft_graph.folder(
            microsoft_graph_drive, folder_name
        )
        self.stdout.write("{}".format(microsoft_graph_folder))
        #
        self.stdout.write(
            "List files in the '{}' folder in drive {}...".format(
                microsoft_graph_folder.name, microsoft_graph_drive.drive_id
            )
        )
        folder_file_list = microsoft_graph.folder_file_list(
            microsoft_graph_drive, microsoft_graph_folder
        )
        for x in folder_file_list:
            self.stdout.write("{}".format(x))
        #
        # file_name = "converted-pdf-file.pdf"
        # file_name = "Decision making flowchart.docx"
        file_name = "climate-emergency-3690-Report-Final-221019.pdf"
        self.stdout.write(
            "Upload '{}' to the '{}' folder in drive {}...".format(
                file_name,
                microsoft_graph_folder.name,
                microsoft_graph_drive.drive_id,
            )
        )
        microsoft_graph_file = microsoft_graph.folder_file_upload(
            microsoft_graph_drive, microsoft_graph_folder, file_name
        )
        self.stdout.write("{}".format(microsoft_graph_file))
        #
        self.stdout.write("{} - Complete".format(self.help))

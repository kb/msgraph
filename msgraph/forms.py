# -*- encoding: utf-8 -*-
from django import forms
from django.conf import settings

from .models import MicrosoftGraphSettings


class MicrosoftGraphConsentForm(forms.ModelForm):
    class Meta:
        model = MicrosoftGraphSettings
        fields = ()

    def clean(self):
        cleaned_data = super().clean()
        if settings.MSGRAPH_APPLICATION_ID and settings.MSGRAPH_CLIENT_SECRET:
            pass
        else:
            message = (
                "You cannot ask for administrator consent until you have set "
                "the application ID and client secret in your settings."
            )
            raise forms.ValidationError(
                message, code="microsoft_graph__consent"
            )
        return cleaned_data

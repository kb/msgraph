# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from msgraph.models import MicrosoftGraphSettings


@pytest.mark.django_db
def test_microsoft_graph_consent(client, settings):
    """

    .. note:: If this test is failing, check your ``HOST_NAME`` environment
              variable is set to ``http://localhost:8000``

    """
    # login
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    settings.MSGRAPH_APPLICATION_ID = "6731de76-14a6-49ae-97bc-6eba6914391e"
    settings.MSGRAPH_CLIENT_SECRET = "my-client-secret"
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    # test
    assert 0 == MicrosoftGraphSettings.objects.count()
    response = client.post(reverse("microsoft.graph.consent"))
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert (
        "https://login.microsoftonline.com/my-tenant-id/adminconsent"
        "?client_id=6731de76-14a6-49ae-97bc-6eba6914391e"
        "&redirect_uri=http://localhost:8000/microsoft/graph/consent/confirm/"
    ) == response.url, (
        "If this test is failing, check your 'HOST_NAME' environment "
        "variable is set to 'http://localhost:8000'"
    )


@pytest.mark.django_db
def test_microsoft_graph_consent_empty_tenant(client, settings):
    """

    .. note:: If this test is failing, check your ``HOST_NAME`` environment
              variable is set to ``http://localhost:8000``

    """
    # login
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    settings.MSGRAPH_APPLICATION_ID = "6731de76-14a6-49ae-97bc-6eba6914391e"
    settings.MSGRAPH_CLIENT_SECRET = "my-client-secret"
    settings.MSGRAPH_TENANT_ID = ""
    # test
    assert 0 == MicrosoftGraphSettings.objects.count()
    response = client.post(reverse("microsoft.graph.consent"))
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert (
        "https://login.microsoftonline.com/common/adminconsent"
        "?client_id=6731de76-14a6-49ae-97bc-6eba6914391e"
        "&redirect_uri=http://localhost:8000/microsoft/graph/consent/confirm/"
    ) == response.url, (
        "If this test is failing, check your 'HOST_NAME' environment "
        "variable is set to 'http://localhost:8000'"
    )


@pytest.mark.django_db
def test_microsoft_graph_consent_get(client, settings):
    """

    .. note:: If this test is failing, check your ``HOST_NAME`` environment
              variable is set to ``http://localhost:8000``

    """
    # login
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    settings.MSGRAPH_APPLICATION_ID = "6731de76-14a6-49ae-97bc-6eba6914391e"
    settings.MSGRAPH_CLIENT_SECRET = "my-client-secret"
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    # test
    response = client.get(reverse("microsoft.graph.consent"))
    # check
    assert HTTPStatus.OK == response.status_code, response.context[
        "form"
    ].errors
    assert "microsoftgraphsettings" in response.context
    assert "redirect_uri" in response.context
    assert (
        "http://localhost:8000/microsoft/graph/consent/confirm/"
        == response.context["redirect_uri"]
    )


@pytest.mark.django_db
def test_microsoft_graph_consent_no_application_id_or_client_secret(
    client, settings
):
    # login
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    settings.MSGRAPH_APPLICATION_ID = ""
    settings.MSGRAPH_CLIENT_SECRET = ""
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    # test
    response = client.post(reverse("microsoft.graph.consent"))
    # check
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            (
                "You cannot ask for administrator consent until you have set "
                "the application ID and client secret in your settings."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_microsoft_graph_consent_confirm(client, settings):
    # setup
    settings.MSGRAPH_APPLICATION_ID = "6731de76-14a6-49ae-97bc-6eba6914391e"
    settings.MSGRAPH_CLIENT_SECRET = "my-client-secret"
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    # login
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # test
    response = client.get(
        (
            "{}"
            "?tenant=a8990e1f-ff32-408a-9f8e-78d3b9139b95&state=12345"
            "&admin_consent=True"
        ).format(reverse("microsoft.graph.consent.confirm"))
    )
    # check
    assert HTTPStatus.OK == response.status_code
    microsoft_graph_settings = MicrosoftGraphSettings.load()
    microsoft_graph_settings.refresh_from_db()
    assert (
        "a8990e1f-ff32-408a-9f8e-78d3b9139b95"
        == microsoft_graph_settings.tenant
    )
    assert user == microsoft_graph_settings.tenant_updated_by
    assert (
        timezone.now().date()
        == microsoft_graph_settings.tenant_updated_date.now().date()
    )

# -*- encoding: utf-8 -*-
import pytest
import uuid

from login.tests.factories import UserFactory
from .factories import MicrosoftGraphGroupFactory
from msgraph.models import MicrosoftGraphGroup


@pytest.mark.django_db
def test_current():
    MicrosoftGraphGroupFactory(name="a")
    microsoft_graph_group = MicrosoftGraphGroupFactory(name="b")
    microsoft_graph_group.set_deleted(UserFactory())
    MicrosoftGraphGroupFactory(name="c")
    assert ["a", "c"] == [x.name for x in MicrosoftGraphGroup.objects.current()]


@pytest.mark.django_db
def test_factory():
    MicrosoftGraphGroupFactory()


@pytest.mark.django_db
def test_init_microsoft_graph_group():
    group_uuid = str(uuid.uuid4())
    x = MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, "Support", "Support Team"
    )
    x.refresh_from_db()
    assert group_uuid == str(x.uuid)
    assert "Support" == x.name
    assert "Support Team" == x.description


@pytest.mark.django_db
def test_init_microsoft_graph_group_exists():
    group_uuid = str(uuid.uuid4())
    MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, "Developers", "Software"
    )
    assert 1 == MicrosoftGraphGroup.objects.count()
    x = MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, "R&D", "Research and Development"
    )
    assert 1 == MicrosoftGraphGroup.objects.count()
    x.refresh_from_db()
    assert group_uuid == str(x.uuid)
    assert "R&D" == x.name
    assert "Research and Development" == x.description


@pytest.mark.django_db
def test_init_microsoft_graph_group_exists_deleted():
    group_uuid = str(uuid.uuid4())
    x = MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, "Developers", "Development Team"
    )
    x.set_deleted(UserFactory())
    x.refresh_from_db()
    assert x.is_deleted is True
    assert 1 == MicrosoftGraphGroup.objects.count()
    x = MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, "R&D", "Research and Development"
    )
    assert 1 == MicrosoftGraphGroup.objects.count()
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_init_microsoft_graph_group_too_long():
    some_long_text = (
        "123456789 123456789 123456789 123456789 123456789 123456789 "
        "123456789 123456789 123456789 123456789 123456789 123456789 "
        "123456789 123456789 123456789+123456789 123456789 123456789 "
    )
    group_uuid = str(uuid.uuid4())
    x = MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, some_long_text, some_long_text
    )
    x.refresh_from_db()
    assert group_uuid == str(x.uuid)
    assert (
        "123456789 123456789 123456789 123456789 123456789 123456789 "
        "123456789 123456789 123456789 123456789 123456789 123456789 "
        "123456789 123456789 123456789+"
    ) == x.name
    assert (
        "123456789 123456789 123456789 123456789 123456789 123456789 "
        "123456789 123456789 123456789 123456789 123456789 123456789 "
        "123456789 123456789 123456789+"
    ) == x.description


@pytest.mark.django_db
def test_str():
    group_uuid = str(uuid.uuid4())
    x = MicrosoftGraphGroup.objects.init_microsoft_graph_group(
        group_uuid, "Developers", "Software Development"
    )
    assert f"Developers ('{group_uuid}')" == str(x)

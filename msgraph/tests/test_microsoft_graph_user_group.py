# -*- encoding: utf-8 -*-
import pytest
import uuid

from django.db import IntegrityError

from login.tests.factories import UserFactory
from .factories import (
    MicrosoftGraphGroupFactory,
    MicrosoftGraphUserFactory,
    MicrosoftGraphUserGroupFactory,
)
from msgraph.models import MicrosoftGraphUserGroup


@pytest.mark.django_db
def test_current():
    MicrosoftGraphUserGroupFactory(group=MicrosoftGraphGroupFactory(name="a"))
    microsoft_user_group = MicrosoftGraphUserGroupFactory(
        group=MicrosoftGraphGroupFactory(name="b")
    )
    microsoft_user_group.set_deleted(UserFactory())
    MicrosoftGraphUserGroupFactory(group=MicrosoftGraphGroupFactory(name="c"))
    assert ["a", "c"] == [
        x.group.name for x in MicrosoftGraphUserGroup.objects.current()
    ]


@pytest.mark.django_db
def test_current_user():
    user = MicrosoftGraphUserFactory()
    MicrosoftGraphUserGroupFactory(
        user=user, group=MicrosoftGraphGroupFactory(name="a")
    )
    microsoft_user_group = MicrosoftGraphUserGroupFactory(user=user)
    microsoft_user_group.set_deleted(UserFactory())
    MicrosoftGraphUserGroupFactory()
    MicrosoftGraphUserGroupFactory(
        user=user, group=MicrosoftGraphGroupFactory(name="c")
    )
    assert ["a", "c"] == [
        x.group.name for x in MicrosoftGraphUserGroup.objects.current(user)
    ]


@pytest.mark.django_db
def test_factory():
    MicrosoftGraphUserGroupFactory()


@pytest.mark.django_db
def test_duplicate():
    group = MicrosoftGraphGroupFactory()
    user = MicrosoftGraphUserFactory()
    MicrosoftGraphUserGroupFactory(user=user, group=group)
    with pytest.raises(IntegrityError) as e:
        MicrosoftGraphUserGroupFactory(user=user, group=group)
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_init_microsoft_graph_user_group():
    group = MicrosoftGraphGroupFactory()
    user = MicrosoftGraphUserFactory()
    x = MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(
        user, group
    )
    x.refresh_from_db()
    assert str(group.uuid) == str(x.group.uuid)
    assert str(user.uuid) == str(x.user.uuid)


@pytest.mark.django_db
def test_init_microsoft_graph_user_group_exists():
    group = MicrosoftGraphGroupFactory()
    user = MicrosoftGraphUserFactory()
    MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(user, group)
    assert 1 == MicrosoftGraphUserGroup.objects.count()
    x = MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(
        user, group
    )
    assert 1 == MicrosoftGraphUserGroup.objects.count()
    x.refresh_from_db()
    assert str(group.uuid) == str(x.group.uuid)
    assert str(user.uuid) == str(x.user.uuid)


@pytest.mark.django_db
def test_init_microsoft_graph_user_group_exists_deleted():
    group = MicrosoftGraphGroupFactory()
    user = MicrosoftGraphUserFactory()
    x = MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(
        user, group
    )
    x.set_deleted(UserFactory())
    x.refresh_from_db()
    assert x.is_deleted is True
    assert 1 == MicrosoftGraphUserGroup.objects.count()
    x = MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(
        user, group
    )
    assert 1 == MicrosoftGraphUserGroup.objects.count()
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_str():
    group = MicrosoftGraphGroupFactory(name="Developers")
    user = MicrosoftGraphUserFactory(email="code@pkimber.net")
    x = MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(
        user, group
    )
    assert (
        f"code@pkimber.net ('{user.uuid}') Developers ('{group.uuid}')"
        == str(x)
    )

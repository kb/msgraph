# -*- encoding: utf-8 -*-
import pytest
import uuid

from login.tests.factories import UserFactory
from .factories import MicrosoftGraphUserFactory
from msgraph.models import MicrosoftGraphUser


@pytest.mark.django_db
def test_current_1():
    MicrosoftGraphUserFactory(last_name="a", email="a@pkimber.net")
    microsoft_graph_user = MicrosoftGraphUserFactory(
        last_name="b", email="b@pkimber.net"
    )
    microsoft_graph_user.set_deleted(UserFactory())
    MicrosoftGraphUserFactory(last_name="c", email="")
    MicrosoftGraphUserFactory(last_name="d", email="d@pkimber.net")
    assert ["a", "d"] == [
        x.last_name for x in MicrosoftGraphUser.objects.current()
    ]


@pytest.mark.django_db
def test_current_2():
    MicrosoftGraphUser.objects.init_microsoft_graph_user(
        str(uuid.uuid4()), "u1@test.com", "u1", "", "", ""
    )
    x = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        str(uuid.uuid4()), "u2@test.com", "u2", "", "", ""
    )
    x.set_deleted(UserFactory())
    MicrosoftGraphUser.objects.init_microsoft_graph_user(
        str(uuid.uuid4()), "u3@test.com", "u3", "", "", ""
    )
    MicrosoftGraphUser.objects.init_microsoft_graph_user(
        str(uuid.uuid4()), "", "u4", "", "", ""
    )
    assert ["u1", "u3"] == [
        x.first_name for x in MicrosoftGraphUser.objects.current()
    ]


@pytest.mark.django_db
def test_factory():
    MicrosoftGraphUserFactory()


@pytest.mark.django_db
def test_init_microsoft_graph_user():
    user_uuid = str(uuid.uuid4())
    x = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid,
        "code@pkimber.net",
        "Patrick",
        "Kimber",
        "Developer",
        "Exeter",
    )
    x.refresh_from_db()
    assert user_uuid == str(x.uuid)
    assert "code@pkimber.net" == x.email
    assert "Patrick" == x.first_name
    assert "Kimber" == x.last_name
    assert "Developer" == x.job_title
    assert "Exeter" == x.office_location


@pytest.mark.django_db
def test_init_microsoft_graph_user_exists():
    user_uuid = str(uuid.uuid4())
    MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid,
        "code@pkimber.net",
        "Patrick",
        "Kimber",
        "Developer",
        "Crediton",
    )
    assert 1 == MicrosoftGraphUser.objects.count()
    x = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid, "web@pkimber.net", "P", "Kim", "Farmer", "Exeter"
    )
    assert 1 == MicrosoftGraphUser.objects.count()
    x.refresh_from_db()
    assert user_uuid == str(x.uuid)
    assert "web@pkimber.net" == x.email
    assert "P" == x.first_name
    assert "Kim" == x.last_name
    assert "Farmer" == x.job_title
    assert "Exeter" == x.office_location


@pytest.mark.django_db
def test_init_microsoft_graph_user_exists_deleted():
    user_uuid = str(uuid.uuid4())
    x = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid,
        "code@pkimber.net",
        "P",
        "Kimber",
        "",
        "",
    )
    x.set_deleted(UserFactory())
    x.refresh_from_db()
    assert x.is_deleted is True
    assert 1 == MicrosoftGraphUser.objects.count()
    x = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid, "web@pkimber.net", "P", "Kimber", "", ""
    )
    assert 1 == MicrosoftGraphUser.objects.count()
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_str():
    user_uuid = str(uuid.uuid4())
    x = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid,
        "code@pkimber.net",
        "Patrick",
        "Kimber",
        "Developer",
        "Crediton",
    )
    assert "code@pkimber.net ('{}')".format(str(user_uuid)) == str(x)

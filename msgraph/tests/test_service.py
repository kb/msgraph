# -*- encoding: utf-8 -*-
import pytest
import responses
import uuid

from django.conf import settings
from http import HTTPStatus
from pathlib import Path
from unittest import mock

from login.tests.factories import UserFactory
from msgraph.models import (
    MicrosoftGraphFileAlreadyExists,
    MicrosoftGraphUser,
    MicrosoftGraphUserSupervisor,
)
from msgraph.service import (
    GraphDrive,
    GraphFolder,
    GraphGroup,
    GraphUser,
    MicrosoftGraph,
    MicrosoftGraphError,
    MicrosoftGraphUserGroup,
    update_microsoft_graph_users,
)
from .factories import (
    MicrosoftGraphGroupFactory,
    MicrosoftGraphSettingsFactory,
    MicrosoftGraphUserFactory,
    MicrosoftGraphUserGroupFactory,
)


@pytest.mark.django_db
@responses.activate
def test_folder_file_upload_cannot_find_file():
    MicrosoftGraphSettingsFactory(tenant="pk")
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    folder_file = Path(
        settings.BASE_DIR,
        settings.MEDIA_ROOT,
        "data",
        "file-does-not-exist.doc",
    )
    document_name = "my-document-name"
    microsoft_graph = MicrosoftGraph()
    with pytest.raises(MicrosoftGraphError) as e:
        microsoft_graph.folder_file_upload(
            GraphDrive(drive_id="my-drive-id"),
            GraphFolder(
                folder_id="my-folder-id",
                name="my-folder-name",
                url="my-folder-url",
            ),
            folder_file,
            document_name,
        )
    assert "Cannot find file to upload" in str(e.value)
    assert "media/data/file-does-not-exist.doc'" in str(e.value)


@pytest.mark.django_db
@responses.activate
def test_folder_file_upload_file_already_exists():
    MicrosoftGraphSettingsFactory(tenant="pk")
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.POST,
        (
            "https://graph.microsoft.com/v1.0/drives/my-drive-id/items/"
            "my-folder-id:/my-document-name:/createUploadSession"
        ),
        json={"error": {"code": "nameAlreadyExists"}},
        status=HTTPStatus.NOT_IMPLEMENTED,
    )
    folder_file = Path(
        settings.BASE_DIR, settings.MEDIA_ROOT, "data", "1-2-3.doc"
    )
    # folder_file = "folder-file"
    document_name = "my-document-name"
    microsoft_graph = MicrosoftGraph()
    with pytest.raises(MicrosoftGraphFileAlreadyExists) as e:
        microsoft_graph.folder_file_upload(
            GraphDrive(drive_id="my-drive-id"),
            GraphFolder(
                folder_id="my-folder-id",
                name="my-folder-name",
                url="my-folder-url",
            ),
            folder_file,
            document_name,
        )
    assert (
        "File 'my-document-name' already exists in the 'my-folder-name' folder"
        in str(e.value)
    )


@pytest.mark.django_db
@responses.activate
def test_group_list():
    MicrosoftGraphSettingsFactory(tenant="pk")
    uuid_group = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups",
        json={
            "value": [
                {
                    "id": uuid_group,
                    "displayName": "Developer",
                    "description": "Software Developer",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    microsoft_graph = MicrosoftGraph()
    result = microsoft_graph.group_list()
    # from rich.pretty import pprint
    # pprint(result, expand_all=True)
    assert [
        GraphGroup(
            group_id=uuid_group,
            name="Developer",
            description="Software Developer",
        )
    ] == result


@pytest.mark.django_db
@responses.activate
def test_group_members():
    MicrosoftGraphSettingsFactory(tenant="pk")
    uuid_user = str(uuid.uuid4())
    uuid_group = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    # members of the active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups/{}/members".format(uuid_group),
        json={
            "value": [
                {
                    "id": uuid_user,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Plymouth",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    microsoft_graph = MicrosoftGraph()
    result = microsoft_graph.group_members(uuid_group)
    # from rich.pretty import pprint
    # pprint(result, expand_all=True)
    assert [
        GraphUser(
            uuid=uuid_user,
            email="a@pkimber.net",
            first_name="A",
            last_name="Aaa",
            job_title="Developer",
            office_location="Plymouth",
            principal_name="A@pkimber.net",
        )
    ] == result


def test_microsoft_graph_user_str():
    x = GraphUser(
        uuid="abc123",
        email="web@pkimber.net",
        first_name="P",
        job_title="my-job-title",
        last_name="Kimber",
        office_location="Hole Court",
        principal_name="WEB@pkimber.net",
    )
    assert (
        "GraphUser("
        "uuid='abc123', "
        "email='web@pkimber.net', "
        "first_name='P', "
        "last_name='Kimber', "
        "job_title='my-job-title', "
        "office_location='Hole Court', "
        "principal_name='WEB@pkimber.net')"
    ) == str(x)


def test_microsoft_graph_user_str_no_email_name():
    x = GraphUser(
        uuid="abc123",
        email=None,
        first_name=None,
        job_title="my-job-title",
        last_name=None,
        office_location="Hole Court",
        principal_name=None,
    )
    assert (
        "GraphUser("
        "uuid='abc123', "
        "email=None, "
        "first_name=None, "
        "last_name=None, "
        "job_title='my-job-title', "
        "office_location='Hole Court', "
        "principal_name=None)"
    ) == str(x)


@pytest.mark.django_db
@responses.activate
def test_update_microsoft_graph_users():
    uuid_manager = str(uuid.uuid4())
    uuid_user_a = str(uuid.uuid4())
    uuid_user_b = str(uuid.uuid4())
    MicrosoftGraphUserFactory(uuid=uuid_manager)
    MicrosoftGraphUserFactory(uuid=uuid_user_b)
    graph_user_to_delete = MicrosoftGraphUserFactory()
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    # users
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users",
        json={
            "value": [
                {
                    "id": uuid_user_a,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Hole Court",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
                {
                    "id": uuid_user_b,
                    "givenName": "B",
                    "jobTitle": "Testing",
                    "mail": "b@pkimber.net",
                    "officeLocation": "Foldhay",
                    "surname": "Baa",
                    "userPrincipalName": "B@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # manager
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users/{}/manager".format(uuid_user_a),
        json={
            "id": uuid_manager,
            "givenName": "M",
            "jobTitle": "Manager",
            "mail": "m@pkimber.net",
            "officeLocation": "Crediton",
            "surname": "Mmm",
            "userPrincipalName": "M@pkimber.net",
        },
        status=HTTPStatus.OK,
    )
    # manager
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users/{}/manager".format(uuid_user_b),
        json={},
        status=HTTPStatus.NOT_FOUND,
    )
    # user groups
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user_a}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 2,
            "value": [
                {
                    "id": "240330d4-847d-460d-956f-7afc2eecbd5c",
                    "deletedDateTime": None,
                    "classification": None,
                    "createdDateTime": "2021-06-18T12:08:44Z",
                    "creationOptions": [],
                    "description": "Software Developers",
                    "displayName": "Developer",
                    "expirationDateTime": None,
                    "groupTypes": [],
                    "isAssignableToRole": None,
                    "mail": None,
                    "mailEnabled": False,
                    "mailNickname": "ca33ae50-2",
                    "membershipRule": None,
                    "membershipRuleProcessingState": None,
                    "onPremisesDomainName": None,
                    "onPremisesLastSyncDateTime": None,
                    "onPremisesNetBiosName": None,
                    "onPremisesSamAccountName": None,
                    "onPremisesSecurityIdentifier": None,
                    "onPremisesSyncEnabled": None,
                    "preferredDataLocation": None,
                    "preferredLanguage": None,
                    "proxyAddresses": [],
                    "renewedDateTime": "2021-06-18T12:08:44Z",
                    "resourceBehaviorOptions": [],
                    "resourceProvisioningOptions": [],
                    "securityEnabled": True,
                    "securityIdentifier": "S-1-12-1-604188884-1175291005-4235882389-1555950638",
                    "theme": None,
                    "visibility": None,
                    "onPremisesProvisioningErrors": [],
                },
                {
                    "id": "89858d9d-508b-4e90-abe4-7dcdaeef6e56",
                    "deletedDateTime": None,
                    "classification": None,
                    "createdDateTime": "2022-08-13T15:45:32Z",
                    "creationOptions": [],
                    "description": "Software Support",
                    "displayName": "Support",
                    "expirationDateTime": None,
                    "groupTypes": [],
                    "isAssignableToRole": None,
                    "mail": None,
                    "mailEnabled": False,
                    "mailNickname": "94f2f884-7",
                    "membershipRule": None,
                    "membershipRuleProcessingState": None,
                    "onPremisesDomainName": None,
                    "onPremisesLastSyncDateTime": None,
                    "onPremisesNetBiosName": None,
                    "onPremisesSamAccountName": None,
                    "onPremisesSecurityIdentifier": None,
                    "onPremisesSyncEnabled": None,
                    "preferredDataLocation": None,
                    "preferredLanguage": None,
                    "proxyAddresses": [],
                    "renewedDateTime": "2022-08-13T15:45:32Z",
                    "resourceBehaviorOptions": [],
                    "resourceProvisioningOptions": [],
                    "securityEnabled": True,
                    "securityIdentifier": "S-1-12-1-2307231133-1318080651-3447579819-1450110894",
                    "theme": None,
                    "visibility": None,
                    "onPremisesProvisioningErrors": [],
                },
            ],
        },
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user_b}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 0,
            "value": [],
        },
        status=HTTPStatus.OK,
    )
    MicrosoftGraphSettingsFactory(tenant="pk")
    # test
    assert graph_user_to_delete.is_deleted is False
    count = update_microsoft_graph_users(UserFactory())
    assert 2 == count
    # check user created
    graph_user_a = MicrosoftGraphUser.objects.get(uuid=uuid_user_a)
    assert "a@pkimber.net" == graph_user_a.email
    assert "Developer" == graph_user_a.job_title
    assert "Hole Court" == graph_user_a.office_location
    # check user groups created
    user_a_group_names = MicrosoftGraphUserGroup.objects.filter(
        user=graph_user_a
    ).values_list("group__name", flat=True)
    assert ["Developer", "Support"] == list(user_a_group_names)
    # check user supervisor
    graph_user_supervisor = MicrosoftGraphUserSupervisor.objects.get(
        user=graph_user_a
    )
    assert uuid_manager == str(graph_user_supervisor.manager.uuid)
    # check user 'b' is deleted (not in the sync group)
    graph_user_b = MicrosoftGraphUser.objects.get(uuid=uuid_user_b)
    graph_user_b.refresh_from_db()
    assert graph_user_b.is_deleted is False
    # check user is not deleted
    graph_user_to_delete.refresh_from_db()
    assert graph_user_to_delete.is_deleted is False


@pytest.mark.django_db
@responses.activate
def test_update_microsoft_graph_users_delete_user():
    """User is not returned in the list of users, so they should be deleted."""
    uuid_group = str(uuid.uuid4())
    uuid_user = str(uuid.uuid4())
    MicrosoftGraphSettingsFactory(tenant="pk")
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users",
        json={"value": []},
        status=HTTPStatus.OK,
    )
    # user group
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 0,
            "value": [],
        },
        status=HTTPStatus.OK,
    )
    # active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups",
        json={
            "value": [
                {
                    "id": uuid_group,
                    "displayName": "AD-GROUP",
                    "description": "AD Group",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # members of the active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups/{}/members".format(uuid_group),
        json={
            "value": [
                {
                    "id": uuid_user,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Plymouth",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users/{}/manager".format(uuid_user),
        json={},
        status=HTTPStatus.NOT_FOUND,
    )
    graph_user = MicrosoftGraphUserFactory(
        uuid=uuid_user, email="a@pkimber.net"
    )
    assert graph_user.is_deleted is False
    count = update_microsoft_graph_users(UserFactory())
    assert 1 == count
    graph_user.refresh_from_db()
    assert graph_user.is_deleted is True


@pytest.mark.django_db
@responses.activate
def test_update_microsoft_graph_users_undelete_deleted_user():
    """Check we un-delete a deleted user."""
    uuid_group = str(uuid.uuid4())
    uuid_user = str(uuid.uuid4())
    MicrosoftGraphSettingsFactory(tenant="pk")
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users",
        json={
            "value": [
                {
                    "id": uuid_user,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Hole Court",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # user group
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 0,
            "value": [],
        },
        status=HTTPStatus.OK,
    )
    # active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups",
        json={
            "value": [
                {
                    "id": uuid_group,
                    "displayName": "AD-GROUP",
                    "description": "AD Group",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # members of the active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups/{}/members".format(uuid_group),
        json={
            "value": [
                {
                    "id": uuid_user,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Plymouth",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users/{}/manager".format(uuid_user),
        json={},
        status=HTTPStatus.NOT_FOUND,
    )
    graph_user = MicrosoftGraphUserFactory(
        uuid=uuid_user, email="a@pkimber.net"
    )
    graph_user.set_deleted(UserFactory())
    graph_user.refresh_from_db()
    assert graph_user.is_deleted is True
    count = update_microsoft_graph_users(UserFactory())
    assert 1 == count
    graph_user.refresh_from_db()
    assert graph_user.is_deleted is False


@pytest.mark.django_db
@responses.activate
def test_update_microsoft_graph_users_deleted_user_group():
    uuid_group = str(uuid.uuid4())
    uuid_user = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    # users
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users",
        json={
            "value": [
                {
                    "id": uuid_user,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Hole Court",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # manager
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user}/manager",
        json={},
        status=HTTPStatus.NOT_FOUND,
    )
    # active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups",
        json={
            "value": [
                {
                    "id": uuid_group,
                    "displayName": "AD-GROUP",
                    "description": "AD Group",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # members of the active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups/{}/members".format(uuid_group),
        json={
            "value": [
                {
                    "id": uuid_user,
                    "givenName": "A",
                    "jobTitle": "Developer",
                    "mail": "a@pkimber.net",
                    "officeLocation": "Plymouth",
                    "surname": "Aaa",
                    "userPrincipalName": "A@pkimber.net",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # user groups
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 1,
            "value": [
                {
                    # "id": "89858d9d-508b-4e90-abe4-7dcdaeef6e56",
                    "id": f"{uuid_group}",
                    "deletedDateTime": None,
                    "classification": None,
                    "createdDateTime": "2022-08-13T15:45:32Z",
                    "creationOptions": [],
                    "description": "Software Support",
                    "displayName": "Support",
                    "expirationDateTime": None,
                    "groupTypes": [],
                    "isAssignableToRole": None,
                    "mail": None,
                    "mailEnabled": False,
                    "mailNickname": "94f2f884-7",
                    "membershipRule": None,
                    "membershipRuleProcessingState": None,
                    "onPremisesDomainName": None,
                    "onPremisesLastSyncDateTime": None,
                    "onPremisesNetBiosName": None,
                    "onPremisesSamAccountName": None,
                    "onPremisesSecurityIdentifier": None,
                    "onPremisesSyncEnabled": None,
                    "preferredDataLocation": None,
                    "preferredLanguage": None,
                    "proxyAddresses": [],
                    "renewedDateTime": "2022-08-13T15:45:32Z",
                    "resourceBehaviorOptions": [],
                    "resourceProvisioningOptions": [],
                    "securityEnabled": True,
                    "securityIdentifier": "S-1-12-1-2307231133-1318080651-3447579819-1450110894",
                    "theme": None,
                    "visibility": None,
                    "onPremisesProvisioningErrors": [],
                },
            ],
        },
        status=HTTPStatus.OK,
    )
    MicrosoftGraphSettingsFactory(tenant="pk")
    # create the user and two groups
    graph_user = MicrosoftGraphUserFactory(uuid=uuid_user)
    MicrosoftGraphUserGroupFactory(
        user=graph_user,
        group=MicrosoftGraphGroupFactory(uuid=uuid_group, name="Support"),
    )
    MicrosoftGraphUserGroupFactory(
        user=graph_user, group=MicrosoftGraphGroupFactory(name="Developer")
    )
    # the 'Developer' group should get soft deleted
    count = update_microsoft_graph_users(UserFactory())
    assert 1 == count
    # check user groups
    graph_user = MicrosoftGraphUser.objects.get(uuid=uuid_user)
    group_names = MicrosoftGraphUserGroup.objects.current(
        graph_user
    ).values_list("group__name", flat=True)
    # The 'Developer' group should get soft deleted
    assert ["Support"] == list(group_names)


@pytest.mark.django_db
@responses.activate
def test_user_groups():
    MicrosoftGraphSettingsFactory(tenant="pk")
    uuid_user = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 2,
            "value": [
                {
                    "id": "240330d4-847d-460d-956f-7afc2eecbd5c",
                    "deletedDateTime": None,
                    "classification": None,
                    "createdDateTime": "2021-06-18T12:08:44Z",
                    "creationOptions": [],
                    "description": "Software Developers",
                    "displayName": "Developer",
                    "expirationDateTime": None,
                    "groupTypes": [],
                    "isAssignableToRole": None,
                    "mail": None,
                    "mailEnabled": False,
                    "mailNickname": "ca33ae50-2",
                    "membershipRule": None,
                    "membershipRuleProcessingState": None,
                    "onPremisesDomainName": None,
                    "onPremisesLastSyncDateTime": None,
                    "onPremisesNetBiosName": None,
                    "onPremisesSamAccountName": None,
                    "onPremisesSecurityIdentifier": None,
                    "onPremisesSyncEnabled": None,
                    "preferredDataLocation": None,
                    "preferredLanguage": None,
                    "proxyAddresses": [],
                    "renewedDateTime": "2021-06-18T12:08:44Z",
                    "resourceBehaviorOptions": [],
                    "resourceProvisioningOptions": [],
                    "securityEnabled": True,
                    "securityIdentifier": "S-1-12-1-604188884-1175291005-4235882389-1555950638",
                    "theme": None,
                    "visibility": None,
                    "onPremisesProvisioningErrors": [],
                },
                {
                    "id": "89858d9d-508b-4e90-abe4-7dcdaeef6e56",
                    "deletedDateTime": None,
                    "classification": None,
                    "createdDateTime": "2022-08-13T15:45:32Z",
                    "creationOptions": [],
                    "description": "Software Support",
                    "displayName": "Support",
                    "expirationDateTime": None,
                    "groupTypes": [],
                    "isAssignableToRole": None,
                    "mail": None,
                    "mailEnabled": False,
                    "mailNickname": "94f2f884-7",
                    "membershipRule": None,
                    "membershipRuleProcessingState": None,
                    "onPremisesDomainName": None,
                    "onPremisesLastSyncDateTime": None,
                    "onPremisesNetBiosName": None,
                    "onPremisesSamAccountName": None,
                    "onPremisesSecurityIdentifier": None,
                    "onPremisesSyncEnabled": None,
                    "preferredDataLocation": None,
                    "preferredLanguage": None,
                    "proxyAddresses": [],
                    "renewedDateTime": "2022-08-13T15:45:32Z",
                    "resourceBehaviorOptions": [],
                    "resourceProvisioningOptions": [],
                    "securityEnabled": True,
                    "securityIdentifier": "S-1-12-1-2307231133-1318080651-3447579819-1450110894",
                    "theme": None,
                    "visibility": None,
                    "onPremisesProvisioningErrors": [],
                },
            ],
        },
        status=HTTPStatus.OK,
    )
    microsoft_graph = MicrosoftGraph()
    result = microsoft_graph.user_groups(uuid_user)
    assert [
        GraphGroup(
            group_id="240330d4-847d-460d-956f-7afc2eecbd5c",
            name="Developer",
            description="Software Developers",
        ),
        GraphGroup(
            group_id="89858d9d-508b-4e90-abe4-7dcdaeef6e56",
            name="Support",
            description="Software Support",
        ),
    ] == result


@pytest.mark.django_db
@responses.activate
def test_user_groups_not_found():
    MicrosoftGraphSettingsFactory(tenant="pk")
    uuid_user = str(uuid.uuid4())
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        f"https://graph.microsoft.com/v1.0/users/{uuid_user}/memberOf/microsoft.graph.group",
        json={
            "@odata.context": "https://graph.microsoft.com/v1.0/$metadata#groups",
            "@odata.count": 0,
            "value": [],
        },
        status=HTTPStatus.NOT_FOUND,
    )
    microsoft_graph = MicrosoftGraph()
    result = microsoft_graph.user_groups(uuid_user)
    assert [] == result


@pytest.mark.django_db
@pytest.mark.parametrize(
    "email",
    [
        "",
        None,
    ],
)
def test_user_no_email(email):
    with mock.patch("msgraph.service.MicrosoftGraphSettings.token"):
        microsoft_graph = MicrosoftGraph()
        with pytest.raises(MicrosoftGraphError) as e:
            microsoft_graph.user(email)
    assert "Cannot get user without an email address" in str(e.value)

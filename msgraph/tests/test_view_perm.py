# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_consent(perm_check):
    url = reverse("microsoft.graph.consent")
    perm_check.admin(url)


@pytest.mark.django_db
def test_consent_confirm(perm_check):
    url = reverse("microsoft.graph.consent.confirm")
    perm_check.admin(url)

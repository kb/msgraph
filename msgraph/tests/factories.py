# -*- encoding: utf-8 -*-
import factory
import uuid

from django.utils import timezone

from login.tests.factories import UserFactory
from msgraph.models import (
    MicrosoftGraphGroup,
    MicrosoftGraphSettings,
    MicrosoftGraphUser,
    MicrosoftGraphUserGroup,
    MicrosoftGraphUserSupervisor,
)


class MicrosoftGraphGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MicrosoftGraphGroup

    @factory.sequence
    def name(n):
        return "name_{}".format(n)

    @factory.sequence
    def uuid(n):
        return str(uuid.uuid4())


class MicrosoftGraphSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MicrosoftGraphSettings

    tenant_updated_by = factory.SubFactory(UserFactory)
    tenant_updated_date = timezone.now()

    @factory.sequence
    def tenant(n):
        return "tenant_{}".format(n)


class MicrosoftGraphUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MicrosoftGraphUser

    @factory.sequence
    def uuid(n):
        return str(uuid.uuid4())


class MicrosoftGraphUserGroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MicrosoftGraphUserGroup

    user = factory.SubFactory(MicrosoftGraphUserFactory)
    group = factory.SubFactory(MicrosoftGraphGroupFactory)


class MicrosoftGraphUserSupervisorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MicrosoftGraphUserSupervisor

    user = factory.SubFactory(UserFactory)
    manager = factory.SubFactory(MicrosoftGraphUserFactory)

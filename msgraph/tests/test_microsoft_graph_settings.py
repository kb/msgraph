# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from login.tests.factories import UserFactory
from msgraph.models import MicrosoftGraphError, MicrosoftGraphSettings
from .factories import MicrosoftGraphSettingsFactory


@pytest.mark.django_db
def test_str():
    assert "my-tenant" == str(MicrosoftGraphSettingsFactory(tenant="my-tenant"))


@pytest.mark.django_db
def test_set_tenant(settings):
    user = UserFactory()
    # setup
    settings.MSGRAPH_APPLICATION_ID = "6731de76-14a6-49ae-97bc-6eba6914391e"
    settings.MSGRAPH_CLIENT_SECRET = "my-client-secret"
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    graph_settings = MicrosoftGraphSettingsFactory()
    graph_settings.set_tenant("xyz", user)
    graph_settings.refresh_from_db()
    assert user == graph_settings.tenant_updated_by
    assert (
        timezone.now().date() == graph_settings.tenant_updated_date.now().date()
    )
    assert "xyz" == graph_settings.tenant


@pytest.mark.django_db
def test_set_tenant_no_application_or_secret(settings):
    # setup
    settings.MSGRAPH_APPLICATION_ID = ""
    settings.MSGRAPH_CLIENT_SECRET = ""
    settings.MSGRAPH_TENANT_ID = "my-tenant-id"
    microsoft_graph_settings = MicrosoftGraphSettings.load()
    with pytest.raises(MicrosoftGraphError) as e:
        microsoft_graph_settings.set_tenant("abc", UserFactory())
    assert (
        "Microsoft Graph has not been initialised. Ask your system "
        "administrator to set the 'MSGRAPH_APPLICATION_ID', "
        "'MSGRAPH_CLIENT_SECRET' and 'MSGRAPH_TENANT_ID' "
        "in the project settings."
    ) in str(e.value)

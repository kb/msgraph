# -*- encoding: utf-8 -*-
import pytest
import uuid

from login.tests.factories import UserFactory
from msgraph.models import MicrosoftGraphUser, MicrosoftGraphUserSupervisor
from msgraph.tests.factories import (
    MicrosoftGraphUserFactory,
    MicrosoftGraphUserSupervisorFactory,
)


@pytest.mark.django_db
def test_current():
    microsoft_graph_user_a = MicrosoftGraphUserFactory(
        last_name="a", email="a@pkimber.net"
    )
    microsoft_graph_user_b = MicrosoftGraphUserFactory(
        last_name="b", email="b@pkimber.net"
    )
    microsoft_graph_user_b.set_deleted(UserFactory())
    microsoft_graph_user_c = MicrosoftGraphUserFactory(last_name="c", email="")
    microsoft_graph_user_d = MicrosoftGraphUserFactory(
        last_name="d", email="d@pkimber.net"
    )
    MicrosoftGraphUserSupervisorFactory(
        user=microsoft_graph_user_a, manager=microsoft_graph_user_d
    )
    MicrosoftGraphUserSupervisorFactory(
        user=microsoft_graph_user_b, manager=microsoft_graph_user_d
    )
    MicrosoftGraphUserSupervisorFactory(
        user=microsoft_graph_user_c, manager=microsoft_graph_user_d
    )
    MicrosoftGraphUserSupervisorFactory(
        user=microsoft_graph_user_d, manager=microsoft_graph_user_a
    )
    assert [("a", "d"), ("d", "a")] == [
        (x.user.last_name, x.manager.last_name)
        for x in MicrosoftGraphUserSupervisor.objects.current()
    ]


@pytest.mark.django_db
def test_init_microsoft_graph_user_supervisor():
    manager_uuid = str(uuid.uuid4())
    manager = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        manager_uuid,
        "code@pkimber.net",
        "Patrick",
        "Kimber",
        "Developer",
        "Crediton",
    )
    user_uuid = str(uuid.uuid4())
    user = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid, "code@pkimber.net", "Patrick", "Kimber", "Developer", ""
    )
    x = MicrosoftGraphUserSupervisor.objects.init_microsoft_graph_user_supervisor(
        user, manager
    )
    x.refresh_from_db()
    assert user_uuid == str(x.user.uuid)
    assert manager_uuid == str(x.manager.uuid)


@pytest.mark.django_db
def test_init_microsoft_graph_user_supervisor_empty():
    user_uuid = str(uuid.uuid4())
    user = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid, "code@pkimber.net", "Patrick", "Kimber", "Developer", ""
    )
    x = MicrosoftGraphUserSupervisor.objects.init_microsoft_graph_user_supervisor(
        user, None
    )
    x.refresh_from_db()
    assert user_uuid == str(x.user.uuid)
    assert x.manager is None


@pytest.mark.django_db
def test_init_microsoft_graph_user_supervisor_exists():
    manager_uuid = str(uuid.uuid4())
    manager_1 = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        manager_uuid, "code@pkimber.net", "Mark", "Kimber", "Manager 1", ""
    )
    manager_2_uuid = str(uuid.uuid4())
    manager_2 = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        manager_2_uuid, "web@pkimber.net", "Patrick", "Kimber", "Manager 2", ""
    )
    user_uuid = str(uuid.uuid4())
    user = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid, "code@pkimber.net", "Patrick", "Kimber", "Developer", ""
    )
    MicrosoftGraphUserSupervisor.objects.init_microsoft_graph_user_supervisor(
        user, manager_1
    )
    x = MicrosoftGraphUserSupervisor.objects.init_microsoft_graph_user_supervisor(
        user, manager_2
    )
    x.refresh_from_db()
    assert manager_2_uuid == str(x.manager.uuid)


@pytest.mark.django_db
def test_str():
    manager_uuid = str(uuid.uuid4())
    manager = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        manager_uuid, "code@pkimber.net", "Patrick", "Kimber", "Developer", ""
    )
    user_uuid = str(uuid.uuid4())
    user = MicrosoftGraphUser.objects.init_microsoft_graph_user(
        user_uuid, "web@pkimber.net", "Mark", "Kimber", "Manager", ""
    )
    x = MicrosoftGraphUserSupervisor.objects.init_microsoft_graph_user_supervisor(
        user, manager
    )
    assert "web@pkimber.net ('{}') code@pkimber.net ('{}')".format(
        str(user_uuid), str(manager_uuid)
    ) == str(x)

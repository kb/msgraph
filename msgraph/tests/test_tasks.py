# -*- encoding: utf-8 -*-
import logging
import pytest
import responses
import uuid

from django.conf import settings
from http import HTTPStatus

from login.models import SYSTEM_GENERATED
from login.tests.factories import UserFactory
from mail.models import Message
from mail.tests.factories import NotifyFactory
from msgraph.tasks import (
    microsoft_graph_users,
    schedule_update_microsoft_graph_users,
)
from .factories import MicrosoftGraphSettingsFactory


@pytest.mark.django_db
@responses.activate
def test_microsoft_graph_users():
    uuid_group = str(uuid.uuid4())
    UserFactory(username=SYSTEM_GENERATED)
    MicrosoftGraphSettingsFactory(tenant="pk")
    responses.add(
        responses.POST,
        "https://login.microsoftonline.com/pk/oauth2/v2.0/token",
        json={"access_token": "PK55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/users",
        json={"value": []},
        status=HTTPStatus.OK,
    )
    # active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups",
        json={
            "value": [
                {
                    "id": uuid_group,
                    "displayName": "kbuk",
                    "description": "AD Group",
                },
            ]
        },
        status=HTTPStatus.OK,
    )
    # members of the active directory group
    responses.add(
        responses.GET,
        "https://graph.microsoft.com/v1.0/groups/{}/members".format(uuid_group),
        json={"value": []},
        status=HTTPStatus.OK,
    )
    assert 0 == microsoft_graph_users()


@pytest.mark.django_db
@responses.activate
def test_microsoft_graph_users_raise_exception(caplog):
    """Check an email is sent if an exception is thrown!"""
    NotifyFactory(email="test@pkimber.net")
    UserFactory(username=SYSTEM_GENERATED)
    with pytest.raises(Exception) as e:
        microsoft_graph_users()
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert 1 == message.mail_set.count()
    mail = message.mail_set.first()
    assert "Cannot 'update_microsoft_graph_users'" in message.description
    assert "Error 'update_microsoft_graph_users'" == message.subject
    assert 0 == mail.mailfield_set.count()
    assert (
        "MicrosoftGraphError, You do not have administrator authorisation"
    ) in str([str(x.msg) for x in caplog.records if x.levelno == logging.ERROR])


@pytest.mark.django_db
def test_schedule_update_microsoft_graph_users():
    assert schedule_update_microsoft_graph_users() is None

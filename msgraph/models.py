# -*- encoding: utf-8 -*-
import json
import logging
import requests

from django.conf import settings
from django.db import models
from django.utils import timezone
from http import HTTPStatus
from reversion import revisions as reversion

from base.model_utils import TimedCreateModifyDeleteModel
from base.singleton import SingletonModel


logger = logging.getLogger(__name__)


class MicrosoftGraphError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class MicrosoftGraphFileAlreadyExists(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class MicrosoftGraphGroupManager(models.Manager):
    def _create_microsoft_graph_group(self, uuid, name, description):
        x = self.model(
            uuid=uuid, name=name or "", description=description or ""
        )
        x.save()
        return x

    def current(self):
        """Exclude deleted users and users with an empty email address."""
        return self.exclude(deleted=True)

    def init_microsoft_graph_group(self, uuid, name, description):
        name = name or ""
        description = description or ""
        name = name[:150]
        description = description[:150]
        try:
            x = self.model.objects.get(uuid=uuid)
            x.name = name
            x.description = description
            if x.is_deleted:
                x.undelete()
            else:
                x.save()
        except self.model.DoesNotExist:
            x = self._create_microsoft_graph_group(uuid, name, description)
        return x


@reversion.register()
class MicrosoftGraphGroup(TimedCreateModifyDeleteModel):
    """Microsoft Graph Group."""

    uuid = models.UUIDField(primary_key=True, editable=False)
    name = models.CharField(max_length=150, blank=True)
    description = models.CharField(max_length=150, blank=True)
    objects = MicrosoftGraphGroupManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Microsoft Graph Group"
        verbose_name_plural = "Microsoft Graph Groups"

    def __str__(self):
        return "{} ('{}')".format(self.name, self.uuid)


@reversion.register()
class MicrosoftGraphSettings(SingletonModel):
    """Microsoft Graph Settings.

    Docs
    https://www.kbsoftware.co.uk/docs/app-msgraph.html

    Get access without a user
    https://docs.microsoft.com/en-us/graph/auth-v2-service

    """

    tenant = models.CharField(max_length=100)
    tenant_updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="+"
    )
    tenant_updated_date = models.DateTimeField()

    class Meta:
        verbose_name = "Microsoft Graph Settings"

    def __str__(self):
        return "{}".format(self.tenant)

    def _check_initialised(self):
        if settings.MSGRAPH_APPLICATION_ID and settings.MSGRAPH_CLIENT_SECRET:
            pass
        else:
            raise MicrosoftGraphError(
                "Microsoft Graph has not been initialised. Ask your system "
                "administrator to set the 'MSGRAPH_APPLICATION_ID', "
                "'MSGRAPH_CLIENT_SECRET' and 'MSGRAPH_TENANT_ID' in the "
                "project settings."
            )

    def _check_tenant(self):
        if self.tenant:
            pass
        else:
            raise MicrosoftGraphError(
                "You do not have administrator authorisation to use "
                "Microsoft Graph.  Click 'Get Consent' in 'Settings'"
            )

    def set_tenant(self, tenant, user):
        self._check_initialised()
        self.tenant = tenant
        self.tenant_updated_by = user
        self.tenant_updated_date = timezone.now()
        self.save()

    def token(self):
        result = None
        self._check_initialised()
        self._check_tenant()
        url = "https://login.microsoftonline.com/{}/oauth2/v2.0/token".format(
            self.tenant
        )
        response = requests.post(
            url,
            data={
                "client_id": settings.MSGRAPH_APPLICATION_ID,
                "scope": "https://graph.microsoft.com/.default",
                "client_secret": settings.MSGRAPH_CLIENT_SECRET,
                "grant_type": "client_credentials",
            },
        )
        data = response.json()
        if response.status_code == HTTPStatus.OK:
            result = data["access_token"]
        else:
            logger.error(json.dumps(response.json(), indent=4))
            raise MicrosoftGraphError(
                "Cannot get access token: {}.  Check log for details...".format(
                    response.status_code
                )
            )
        return result


class MicrosoftGraphUserManager(models.Manager):
    def _create_microsoft_graph_user(
        self, uuid, email, first_name, last_name, job_title, office_location
    ):
        x = self.model(
            uuid=uuid,
            email=email or "",
            first_name=first_name or "",
            last_name=last_name or "",
            job_title=job_title or "",
            office_location=office_location or "",
        )
        x.save()
        return x

    def current(self):
        """Exclude deleted users and users with an empty email address."""
        return (
            self.exclude(deleted=True)
            .exclude(email__isnull=True)
            .exclude(email__exact="")
        )

    def init_microsoft_graph_user(
        self, uuid, email, first_name, last_name, job_title, office_location
    ):
        try:
            x = self.model.objects.get(uuid=uuid)
            x.email = email or ""
            x.first_name = first_name or ""
            x.last_name = last_name or ""
            x.job_title = job_title or ""
            x.office_location = office_location or ""
            if x.is_deleted:
                x.undelete()
            else:
                x.save()
        except self.model.DoesNotExist:
            x = self._create_microsoft_graph_user(
                uuid, email, first_name, last_name, job_title, office_location
            )
        return x


@reversion.register()
class MicrosoftGraphUser(TimedCreateModifyDeleteModel):
    """Microsoft Graph User."""

    uuid = models.UUIDField(primary_key=True, editable=False)
    email = models.EmailField()
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    job_title = models.CharField(max_length=100, blank=True)
    office_location = models.CharField(max_length=150, blank=True)
    objects = MicrosoftGraphUserManager()

    class Meta:
        ordering = ["email"]
        verbose_name = "Microsoft Graph User"
        verbose_name_plural = "Microsoft Graph Users"

    def __str__(self):
        return "{} ('{}')".format(self.email, self.uuid)


class MicrosoftGraphUserGroupManager(models.Manager):
    def _create_microsoft_graph_user_group(self, user, group):
        x = self.model(user=user, group=group)
        x.save()
        return x

    def current(self, user=None):
        qs = self.exclude(deleted=True)
        if user:
            qs = qs.filter(user=user)
        return qs

    def init_microsoft_graph_user_group(self, user, group):
        try:
            x = self.model.objects.get(user=user, group=group)
            if x.is_deleted:
                x.undelete()
        except self.model.DoesNotExist:
            x = self._create_microsoft_graph_user_group(user, group)
        return x


@reversion.register()
class MicrosoftGraphUserGroup(TimedCreateModifyDeleteModel):
    """Microsoft Graph - User Group."""

    user = models.ForeignKey(MicrosoftGraphUser, on_delete=models.CASCADE)
    group = models.ForeignKey(MicrosoftGraphGroup, on_delete=models.CASCADE)
    objects = MicrosoftGraphUserGroupManager()

    class Meta:
        unique_together = ("user", "group")
        verbose_name = "Microsoft Graph User Group"
        verbose_name_plural = "Microsoft Graph User Groups"

    def __str__(self):
        return "{} ('{}') {} ('{}')".format(
            self.user.email, self.user.uuid, self.group.name, self.group.uuid
        )


class MicrosoftGraphUserSupervisorManager(models.Manager):
    def _create_microsoft_graph_user_supervisor(self, user, manager):
        x = self.model(user=user, manager=manager)
        x.save()
        return x

    def current(self):
        user_pks = [
            str(x)
            for x in MicrosoftGraphUser.objects.current().values_list(
                "pk", flat=True
            )
        ]
        return (
            self.exclude(deleted=True)
            .filter(user__pk__in=user_pks)
            .filter(manager__pk__in=user_pks)
        )

    def init_microsoft_graph_user_supervisor(self, user, manager):
        try:
            x = self.model.objects.get(user=user)
            x.manager = manager
            x.save()
        except self.model.DoesNotExist:
            x = self._create_microsoft_graph_user_supervisor(user, manager)
        return x


@reversion.register()
class MicrosoftGraphUserSupervisor(TimedCreateModifyDeleteModel):
    """Manager for the Microsoft Graph User."""

    user = models.OneToOneField(MicrosoftGraphUser, on_delete=models.CASCADE)
    manager = models.ForeignKey(
        MicrosoftGraphUser,
        related_name="manager",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    objects = MicrosoftGraphUserSupervisorManager()

    class Meta:
        verbose_name = "Microsoft Graph User Supervisor"
        verbose_name_plural = "Microsoft Graph User Supervisors"

    def __str__(self):
        return "{} ('{}') {} ('{}')".format(
            self.user.email,
            self.user.uuid,
            self.manager.email if self.manager else "",
            self.manager.uuid if self.manager else "",
        )

# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    MicrosoftGraphConsentConfirmTemplateView,
    MicrosoftGraphConsentUpdateView,
)


urlpatterns = [
    re_path(
        r"^consent/$",
        MicrosoftGraphConsentUpdateView.as_view(),
        name="microsoft.graph.consent",
    ),
    re_path(
        r"^consent/confirm/$",
        MicrosoftGraphConsentConfirmTemplateView.as_view(),
        name="microsoft.graph.consent.confirm",
    ),
]

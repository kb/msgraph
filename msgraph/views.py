# -*- encoding: utf-8 -*-
import logging
import urllib.parse

from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import UpdateView, TemplateView

from base.view_utils import BaseMixin
from msgraph.forms import MicrosoftGraphConsentForm
from msgraph.models import MicrosoftGraphSettings


logger = logging.getLogger(__name__)


class MicrosoftGraphConsentConfirmTemplateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "msgraph/consent_confirm.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        admin_consent = self.request.GET.get("admin_consent")
        tenant = self.request.GET.get("tenant")
        result = bool(admin_consent == "True")
        if result and tenant:
            msgraph_settings = MicrosoftGraphSettings.load()
            msgraph_settings.set_tenant(tenant, self.request.user)
            message = "Administrator consent succeeded"
            messages.info(self.request, message)
        else:
            message = "Administrator consent failed"
            messages.error(self.request, message)
        context.update(dict(result=result, message=message))
        return context


class MicrosoftGraphConsentUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):
    """

    From, Get access without a user - Get administrator consent:
    https://docs.microsoft.com/en-us/graph/auth-v2-service#3-get-administrator-consent

    """

    form_class = MicrosoftGraphConsentForm
    template_name = "msgraph/consent_form.html"

    def _redirect_uri(self):
        return urllib.parse.urljoin(
            settings.HOST_NAME, reverse("microsoft.graph.consent.confirm")
        )

    def form_valid(self, form):
        """Redirect to the Microsoft consent page.

        Do not save the ``MicrosoftGraphSettings`` model.
        It is updated in the ``MicrosoftGraphConsentConfirmTemplateView``.

        """
        self.object = form.save(commit=False)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        msgraph_settings = MicrosoftGraphSettings.load()
        context.update(
            dict(
                msgraph_settings=msgraph_settings,
                redirect_uri=self._redirect_uri(),
            )
        )
        return context

    def get_object(self):
        return MicrosoftGraphSettings.load()

    def get_success_url(self):
        return (
            "https://login.microsoftonline.com/{}/adminconsent"
            "?client_id={}"
            "&redirect_uri={}"
        ).format(
            settings.MSGRAPH_TENANT_ID or "common",
            settings.MSGRAPH_APPLICATION_ID,
            self._redirect_uri(),
        )

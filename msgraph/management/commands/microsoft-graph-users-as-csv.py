# -*- encoding: utf-8 -*-
import csv

from django.core.management.base import BaseCommand

from msgraph.models import MicrosoftGraphUser


class Command(BaseCommand):
    help = "Microsoft Graph Users as CSV"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = 0
        with open("microsoft-graph-users-as-csv.csv", "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            for x in MicrosoftGraphUser.objects.all().order_by("email"):
                count = count + 1
                csv_writer.writerow([x.email, x.job_title])
        self.stdout.write(
            "{} - Complete - found {} users".format(self.help, count)
        )

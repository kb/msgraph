# -*- encoding: utf-8 -*-
import csv

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from rich.pretty import pprint

from msgraph.service import MicrosoftGraph


class Command(BaseCommand):
    help = "Microsoft Graph Groups as CSV"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        count = 0
        with open("microsoft-graph-groups-as-csv.csv", "w", newline="") as out:
            csv_writer = csv.writer(out, dialect="excel-tab")
            microsoft_graph = MicrosoftGraph()
            group_list = microsoft_graph.group_list()
            group_to_sync = None
            pprint(group_list, expand_all=True)
            for x in group_list:
                count = count + 1
                if x.name == settings.MSGRAPH_GROUP_NAME_TO_SYNC:
                    group_to_sync = x
                csv_writer.writerow([x.name, x.description])
            if group_to_sync:
                print("\n\nGroup to sync:")
                pprint(group_to_sync, expand_all=True)
                group_members = microsoft_graph.group_members(x.group_id)
                print("\n\nGroup members:")
                pprint(group_members, expand_all=True)
            else:
                CommandError(
                    "Cannot find group to sync.  Looking for '{}'".format(
                        settings.MSGRAPH_GROUP_NAME_TO_SYNC
                    )
                )
        self.stdout.write(
            "{} - Complete - found {} groups".format(self.help, count)
        )

# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from msgraph.service import MicrosoftGraph


class Command(BaseCommand):
    help = "Microsoft Graph User List"

    def handle(self, *args, **options):
        """List of users retrieved from the Microsoft Graph API.

        .. tip:: On some systems, the ``principal_name``
                 (``userPrincipalName``) contains the user email, so we
                 retrieve it in case it is needed in future.
                 https://www.kbsoftware.co.uk/crm/ticket/6780/

        """
        self.stdout.write(self.help)
        microsoft_graph = MicrosoftGraph()
        self.stdout.write("List of all users...")
        user_list = microsoft_graph.user_list()
        for count, user in enumerate(user_list, 1):
            self.stdout.write(
                f"{count}. "
                f"{user.uuid} "
                f"{user.email} "
                f"{user.first_name} "
                f"{user.last_name} "
                f"{user.principal_name}"
            )
        self.stdout.write(f"Complete: {self.help}")

# -*- encoding: utf-8 -*-
import json

from django.core.management.base import BaseCommand

from msgraph.service import MicrosoftGraph


class Command(BaseCommand):
    help = (
        "Test the Microsoft Graph User group service "
        "(try testing with 'patrick.kimber@kbuk.onmicrosoft.com')"
    )

    def add_arguments(self, parser):
        parser.add_argument("email", nargs="+", type=str)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        from rich.pretty import pprint

        microsoft_graph = MicrosoftGraph()
        for email in options["email"]:
            self.stdout.write("{}".format(email))
            # user
            microsoft_graph_user = microsoft_graph.user(email)
            pprint(microsoft_graph_user, expand_all=True)
            # group
            microsoft_graph_user_groups = microsoft_graph.user_groups(
                microsoft_graph_user.uuid
            )
            print()
            pprint(microsoft_graph_user_groups, expand_all=True)
            # write to json
            # email_as_slug = email.replace("@", "-").replace(".", "-")
            # file_name = f"microsoft-graph-user-groups-{email_as_slug}.json"
            # with open(file_name, "w") as f:
            #    json.dump(microsoft_graph_user_groups, f, indent=4)
            # self.stdout.write(f"\nDump file:\n{file_name}\n\n")

        self.stdout.write("{} - Complete".format(self.help))

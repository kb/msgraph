# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from msgraph.service import MicrosoftGraph


class Command(BaseCommand):
    help = (
        "Test the Microsoft Graph User service "
        "(try testing with 'patrick.kimber@kbuk.onmicrosoft.com')"
    )

    def add_arguments(self, parser):
        parser.add_argument("email", nargs="+", type=str)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        from rich.pretty import pprint

        microsoft_graph = MicrosoftGraph()
        for email in options["email"]:
            microsoft_graph_user = microsoft_graph.user(email)
            pprint(microsoft_graph_user, expand_all=True)
        self.stdout.write("{} - Complete".format(self.help))

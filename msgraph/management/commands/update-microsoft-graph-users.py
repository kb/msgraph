# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from login.models import SYSTEM_GENERATED
from msgraph.models import MicrosoftGraphUser
from msgraph.service import update_microsoft_graph_users


class Command(BaseCommand):
    help = "Update Microsoft Graph Users"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        user = get_user_model().objects.get(username=SYSTEM_GENERATED)
        count = update_microsoft_graph_users(user)
        self.stdout.write("Records in 'MicrosoftGraphUser'")
        for x in MicrosoftGraphUser.objects.all():
            self.stdout.write(
                "{}  {}  {}".format(
                    str(x.uuid), x.email, "(Deleted)" if x.deleted else ""
                )
            )
        self.stdout.write(
            "{} - Complete - Found {} users".format(self.help, count or 0)
        )

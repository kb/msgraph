# -*- encoding: utf-8 -*-
import attr
import json
import logging
import os
import requests
import urllib.parse

from django.conf import settings
from http import HTTPStatus

from msgraph.models import (
    MicrosoftGraphError,
    MicrosoftGraphFileAlreadyExists,
    MicrosoftGraphGroup,
    MicrosoftGraphSettings,
    MicrosoftGraphUser,
    MicrosoftGraphUserGroup,
    MicrosoftGraphUserSupervisor,
)


logger = logging.getLogger(__name__)


@attr.s
class GraphDrive:
    drive_id = attr.ib()


@attr.s
class GraphFile:
    created = attr.ib()
    file_id = attr.ib()
    mime_type = attr.ib()
    modified = attr.ib()
    name = attr.ib()
    size = attr.ib()
    url = attr.ib()


@attr.s
class GraphFolder:
    folder_id = attr.ib()
    name = attr.ib()
    url = attr.ib()


@attr.s
class GraphGroup:
    group_id = attr.ib()
    name = attr.ib()
    description = attr.ib()


@attr.s()
class GraphUser:
    uuid = attr.ib()
    email = attr.ib()
    first_name = attr.ib()
    last_name = attr.ib()
    job_title = attr.ib()
    office_location = attr.ib()
    principal_name = attr.ib()


def _delete_missing_user_uuids(uuids_in_list, system_user):
    """Delete Microsoft Graph users who are not in the list."""
    user_uuids_to_delete = list(
        MicrosoftGraphUser.objects.current()
        .exclude(uuid__in=uuids_in_list)
        .values_list("uuid", flat=True)
    )
    for uuid in user_uuids_to_delete:
        microsoft_graph_user = MicrosoftGraphUser.objects.get(uuid=uuid)
        microsoft_graph_user.set_deleted(system_user)
    return len(user_uuids_to_delete)


def _find_groups_for_each_user(microsoft_graph, system_user):
    """Find the groups for each of the current users."""
    user_uuids = [x.uuid for x in MicrosoftGraphUser.objects.current()]
    for uuid in user_uuids:
        microsoft_graph_user = MicrosoftGraphUser.objects.get(uuid=uuid)
        user_groups = microsoft_graph.user_groups(microsoft_graph_user.uuid)
        user_group_pks = []
        for group in user_groups:
            microsoft_graph_group = (
                MicrosoftGraphGroup.objects.init_microsoft_graph_group(
                    group.group_id, group.name, group.description
                )
            )
            microsoft_graph_user_group = (
                MicrosoftGraphUserGroup.objects.init_microsoft_graph_user_group(
                    microsoft_graph_user, microsoft_graph_group
                )
            )
            user_group_pks.append(microsoft_graph_user_group.pk)
        # check to see if a user has been removed from a group
        pks_to_delete = (
            MicrosoftGraphUserGroup.objects.current(microsoft_graph_user)
            .exclude(pk__in=user_group_pks)
            .values_list("pk", flat=True)
        )
        # if the user has been removed, then soft-delete
        for pk in pks_to_delete:
            x = MicrosoftGraphUserGroup.objects.get(pk=pk)
            x.set_deleted(system_user)


def _find_manager_for_each_user(microsoft_graph):
    """Find the manager for each of the current users."""
    user_uuids = [x.uuid for x in MicrosoftGraphUser.objects.current()]
    for uuid in user_uuids:
        user = MicrosoftGraphUser.objects.get(uuid=uuid)
        graph_manager = microsoft_graph.user_manager(uuid)
        manager = None
        if graph_manager:
            manager = MicrosoftGraphUser.objects.get(uuid=graph_manager.uuid)
        MicrosoftGraphUserSupervisor.objects.init_microsoft_graph_user_supervisor(
            user, manager
        )


def update_microsoft_graph_users(system_user):
    """Update 'MicrosoftGraphUser' and 'MicrosoftGraphUserSupervisor'.

    1. Get all the users in the Active Directory
    2. Initialise users
    3. Delete users who are not in the list of *users to synchronise*
    4. Find the groups for each user
    5. Find the manager for each user

    """
    count = 0
    uuids_in_list = []
    microsoft_graph = MicrosoftGraph()
    user_list = microsoft_graph.user_list()
    for user in user_list:
        uuids_in_list.append(user.uuid)
        MicrosoftGraphUser.objects.init_microsoft_graph_user(
            user.uuid,
            user.email,
            user.first_name,
            user.last_name,
            user.job_title,
            user.office_location,
        )
        count = count + 1
    count = count + _delete_missing_user_uuids(uuids_in_list, system_user)
    _find_groups_for_each_user(microsoft_graph, system_user)
    _find_manager_for_each_user(microsoft_graph)
    return count


class MicrosoftGraph:
    """Microsoft Graph.

    To build a server side connector, we need to add / remove documents from a
    users OneDrive folder:
    https://www.kbsoftware.co.uk/crm/ticket/4623/

    Our test code for the Microsoft Graph is here::

      example_msgraph/management/commands/microsoft_graph_test.py

    This class is my initial attempt to put the code into a service.

    """

    GRAPH_URL = "https://graph.microsoft.com/v1.0"

    def __init__(self):
        microsoft_graph_settings = MicrosoftGraphSettings.load()
        self.token = microsoft_graph_settings.token()

    def _create_upload_session(
        self, microsoft_graph_drive, microsoft_graph_folder, document_name
    ):
        result = None
        response = requests.post(
            "{}/drives/{}/items/{}:/{}:/createUploadSession".format(
                self.GRAPH_URL,
                microsoft_graph_drive.drive_id,
                microsoft_graph_folder.folder_id,
                document_name,
            ),
            json={"item": {"@microsoft.graph.conflictBehavior": "fail"}},
            headers=self._headers(),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            result = data["uploadUrl"]
        else:
            code = None
            data = response.json()
            error = data.get("error")
            if error:
                code = error.get("code")
            # https://docs.microsoft.com/en-us/graph/api/driveitem-createuploadsession?view=graph-rest-1.0#request-body
            if code == "nameAlreadyExists":
                raise MicrosoftGraphFileAlreadyExists(
                    "File '{}' already exists in the '{}' folder".format(
                        document_name, microsoft_graph_folder.name
                    )
                )
            else:
                message = self._error_message(response)
                raise MicrosoftGraphError(
                    "Cannot create upload session for folder '{}': {}".format(
                        microsoft_graph_folder.name, message
                    )
                )
        return result

    def _error_message(self, response):
        error = message = result = ""
        try:
            data = response.json()
            error = data.get("error")
        except json.JSONDecodeError:
            message = "JSONDecodeError"
        if error:
            code = error.get("code", "")
            message = error.get("message", "")
            result = f"status code {response.status_code}: '{code}' {message}"
        else:
            result = f"status code {response.status_code}: {message}"
        return result

    def _file_from_json(self, row):
        file_info = row.get("file")
        file_system_info = row["fileSystemInfo"]
        return GraphFile(
            file_id=row["id"],
            created=file_system_info["createdDateTime"],
            modified=file_system_info["lastModifiedDateTime"],
            url=row["webUrl"],
            mime_type=file_info["mimeType"],
            name=row["name"],
            size=row["size"],
        )

    def _group_from_json(self, row):
        return GraphGroup(
            group_id=row["id"],
            name=row["displayName"],
            description=row["description"],
        )

    def _headers(self):
        return {"Authorization": "Bearer {}".format(self.token)}

    def _manager_from_json(self, row):
        return GraphUserManager(
            uuid=row["id"],
            email=row["mail"],
            first_name=row["givenName"],
            last_name=row["surname"],
            job_title=row["jobTitle"],
        )

    def _user_from_json(self, row):
        return GraphUser(
            uuid=row["id"],
            email=row["mail"],
            first_name=row["givenName"],
            last_name=row["surname"],
            job_title=row["jobTitle"],
            office_location=row["officeLocation"],
            principal_name=row["userPrincipalName"],
        )

    def file_delete(self, microsoft_graph_drive, item_id):
        """Delete a file."""
        result = None
        logger.debug("file_delete: '{}'".format(item_id))
        url = "{}/drives/{}/items/{}".format(
            self.GRAPH_URL, microsoft_graph_drive.drive_id, item_id
        )
        response = requests.delete(url, headers=self._headers())
        if HTTPStatus.NO_CONTENT == response.status_code:
            logger.info("file_delete: deleted '{}'".format(item_id))
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Item {} in drive {}: {}".format(
                    item_id,
                    microsoft_graph_drive,
                    message,
                )
            )
        return result

    def file_download(
        self, microsoft_graph_drive, item_id, folder_name, document_format=None
    ):
        microsoft_graph_file = self.file_info(microsoft_graph_drive, item_id)
        file_name = microsoft_graph_file.name
        logger.info("file_download: '{}' to {}".format(item_id, file_name))
        download_format = ""
        if document_format:
            download_format = "?format={}".format(document_format)
            file_name = "{}.{}".format(file_name, document_format)
        url = "{}/drives/{}/items/{}/content{}".format(
            self.GRAPH_URL,
            microsoft_graph_drive.drive_id,
            item_id,
            download_format,
        )
        response = requests.get(
            url, headers=self._headers(), allow_redirects=True
        )
        if HTTPStatus.OK == response.status_code:
            with open(os.path.join(folder_name, file_name), "wb") as f:
                f.write(response.content)
        else:
            message = self._error_message(response)
            logger.info(response.text)
            raise MicrosoftGraphError(
                "Cannot download {} in drive {} (format {}): {}".format(
                    item_id,
                    microsoft_graph_drive,
                    message,
                    document_format or "",
                )
            )
        return file_name

    def file_info(self, microsoft_graph_drive, item_id):
        """File information."""
        result = None
        logger.info("file_info: '{}'".format(item_id))
        url = "{}/drives/{}/items/{}".format(
            self.GRAPH_URL, microsoft_graph_drive.drive_id, item_id
        )
        response = requests.get(url, headers=self._headers())
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            is_file = data.get("file")
            if is_file:
                result = self._file_from_json(data)
            else:
                raise MicrosoftGraphError(
                    "Item {} in drive {} is not a file".format(
                        item_id, microsoft_graph_drive
                    )
                )
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Item {} in drive {} is not a file: {}".format(
                    item_id, microsoft_graph_drive, message
                )
            )
        return result

    def folder(self, microsoft_graph_drive, folder_name):
        result = None
        response = requests.get(
            "{}/drives/{}/root:/{}".format(
                self.GRAPH_URL, microsoft_graph_drive.drive_id, folder_name
            ),
            headers=self._headers(),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            result = GraphFolder(
                folder_id=data["id"], name=data["name"], url=data["webUrl"]
            )
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Cannot find the '{}' folder in the drive '{}': {}".format(
                    folder_name, microsoft_graph_drive.drive_id, message
                )
            )
        return result

    def folder_create_if_not_exists(self, microsoft_graph_drive, folder_name):
        """Create a folder in a drive folder.

        To get the ID for the folder, call the ``folder`` method after this
        method.

        https://docs.microsoft.com/en-gb/graph/api/driveitem-post-children

        POST /me/drive/root/children
        Content-Type: application/json

        {
            "name": "New Folder",
            "folder": { },
            "@microsoft.graph.conflictBehavior": "rename"
        }

        """
        result = None
        data = {
            "name": folder_name,
            "folder": {},
            "@microsoft.graph.conflictBehavior": "fail",
        }
        url = "{}/drives/{}/root/children".format(
            self.GRAPH_URL, microsoft_graph_drive.drive_id
        )
        response = requests.post(url, json=data, headers=self._headers())
        if HTTPStatus.CREATED == response.status_code:
            pass
        else:
            code = None
            data = response.json()
            error = data.get("error")
            if error:
                code = error.get("code")
            # https://docs.microsoft.com/en-us/graph/api/resources/driveitem?view=graph-rest-1.0#instance-attributes
            if code == "nameAlreadyExists":
                pass
            else:
                message = self._error_message(response)
                raise MicrosoftGraphError(
                    "Cannot list the contents of the drive '{}': {}".format(
                        microsoft_graph_drive.drive_id, message
                    )
                )
        return result

    def folder_file_list(self, microsoft_graph_drive, microsoft_graph_folder):
        """List of files in a folder."""
        result = []
        response = requests.get(
            "{}/drives/{}/items/{}/children".format(
                self.GRAPH_URL,
                microsoft_graph_drive.drive_id,
                microsoft_graph_folder.folder_id,
            ),
            headers=self._headers(),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            values = data["value"]
            for row in values:
                is_file = row.get("file")
                if is_file:
                    result.append(self._file_from_json(row))
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Cannot list the contents of the drive '{}': {}".format(
                    microsoft_graph_drive.drive_id, message
                )
            )
        return result

    def folder_file_upload(
        self,
        microsoft_graph_drive,
        microsoft_graph_folder,
        folder_file,
        document_name,
    ):
        result = None
        if not os.path.exists(folder_file):
            raise MicrosoftGraphError(
                "Cannot find file to upload '{}'".format(folder_file)
            )
        url = self._create_upload_session(
            microsoft_graph_drive,
            microsoft_graph_folder,
            document_name,
        )
        file_size = os.path.getsize(folder_file)
        with open(folder_file, "rb") as f:
            chunk = 327680
            total = 0
            while True:
                data = f.read(chunk)
                total = total + len(data)
                if data:
                    headers = self._headers()
                    headers.update(
                        {
                            "Content-Length": str(len(data)),
                            "Content-Range": "bytes {}-{}/{}".format(
                                total - len(data), total - 1, file_size
                            ),
                        }
                    )
                    response = requests.put(url, data=data, headers=headers)
                    if response.status_code == HTTPStatus.ACCEPTED:
                        # there are more byte ranges that need to be uploaded
                        pass
                    elif (
                        response.status_code == HTTPStatus.CREATED
                        or response.status_code == HTTPStatus.OK
                    ):
                        # the last byte range of a file is received
                        data = response.json()
                        result = self._file_from_json(data)
                    else:
                        message = self._error_message(response)
                        raise MicrosoftGraphError(
                            "Cannot upload '{}' to '{}': {}".format(
                                folder_file,
                                microsoft_graph_folder.name,
                                message,
                            )
                        )
                else:
                    break
        return result

    def group_list(self):
        """List of all the groups."""
        result = []
        url = "{}/groups".format(self.GRAPH_URL)
        response = requests.get(url, headers=self._headers())
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            rows = data["value"]
            for x in rows:
                result.append(self._group_from_json(x))
        else:
            raise MicrosoftGraphError(
                "Cannot get a list of groups: {}".format(
                    self._error_message(response)
                )
            )
        return result

    def group_members(self, group_id):
        """Members of the group (users)."""
        result = []
        url = "{}/groups/{}/members".format(self.GRAPH_URL, group_id)
        while url:
            response = requests.get(url, headers=self._headers())
            if HTTPStatus.OK == response.status_code:
                data = response.json()
                rows = data["value"]
                for x in rows:
                    result.append(self._user_from_json(x))
                url = data.get("@odata.nextLink")
            else:
                raise MicrosoftGraphError(
                    "Cannot get a list of users in the group: {}".format(
                        self._error_message(response)
                    )
                )
        return result

    def user(self, email):
        """Find the user by email address.

        Arguments:
        email -- email address of the user

        .. warning:: This is the ``userPrincipalName`` in the API, so need to
                     verify this is the same thing!

        .. tip:: On some systems, the ``userPrincipalName`` contains the user
                 email, so we will retrieve it in case it is needed in future.
                 https://www.kbsoftware.co.uk/crm/ticket/6780/

        """
        if not email:
            raise MicrosoftGraphError(
                "Cannot get user without an email address: '{}'".format(email)
            )
        response = requests.get(
            (
                f"{self.GRAPH_URL}/users/{urllib.parse.quote(email)}?"
                "$select=id,jobTitle,mail,givenName,officeLocation,surname,"
                "userPrincipalName"
            ),
            headers=self._headers(),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            result = self._user_from_json(data)
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Cannot get user with email '{}': {}".format(email, message)
            )
        return result

    def user_groups(self, user_id):
        """List of security groups for a user.

        How to list membership of only security groups from Microsoft Graph
        https://github.com/microsoftgraph/microsoft-graph-docs/issues/10891#issuecomment-748239185

        """
        result = []
        if not user_id:
            raise MicrosoftGraphError(
                "Cannot get user groups without a 'user_id': '{}'".format(
                    user_id
                )
            )
        headers = self._headers()
        headers.update({"ConsistencyLevel": "eventual"})
        response = requests.get(
            "{}/users/{}/memberOf/microsoft.graph.group?$count=true&$filter=(securityEnabled eq true)".format(
                self.GRAPH_URL, user_id
            ),
            headers=headers,
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            rows = data["value"]
            for x in rows:
                result.append(self._group_from_json(x))
        elif HTTPStatus.NOT_FOUND == response.status_code:
            # e.g. status code 404: 'Request_ResourceNotFound'
            # Resource 'f1234' does not exist or one of its queried
            # reference-property objects are not present.
            pass
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Cannot get the group details for user '{}': {}".format(
                    user_id, message
                )
            )
        return result

    def user_drive(self, microsoft_graph_user):
        result = None
        response = requests.get(
            "{}/users/{}/drive".format(
                self.GRAPH_URL, microsoft_graph_user.uuid
            ),
            headers=self._headers(),
        )
        if HTTPStatus.OK == response.status_code:
            data = response.json()
            result = GraphDrive(drive_id=data["id"])  # , url=data["webUrl"])
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Cannot get the drive for a user '{}' ({}): {}".format(
                    microsoft_graph_user.uuid,
                    microsoft_graph_user.email,
                    message,
                )
            )
        return result

    def user_list(self):
        """List of all the users.

        .. tip:: On some systems, the ``userPrincipalName`` contains the user
                 email, so we will retrieve it in case it is needed in future.
                 https://www.kbsoftware.co.uk/crm/ticket/6780/

        """
        result = []
        url = (
            f"{self.GRAPH_URL}/users?$select=id,jobTitle,mail,"
            "givenName,officeLocation,surname,userPrincipalName"
        )
        while url:
            response = requests.get(url, headers=self._headers())
            if HTTPStatus.OK == response.status_code:
                data = response.json()
                rows = data["value"]
                for x in rows:
                    result.append(self._user_from_json(x))
                url = data.get("@odata.nextLink")
            else:
                raise MicrosoftGraphError(
                    "Cannot get a list of users: {}".format(
                        self._error_message(response)
                    )
                )
        return result

    def user_manager(self, user_id):
        """The manager for a user.

        How to get manager property from a users request
        https://stackoverflow.com/questions/43328466/how-to-get-manager-property-from-a-users-request

        """
        result = None
        if not user_id:
            raise MicrosoftGraphError(
                "Cannot get the users manager without "
                "a 'user_id': '{}'".format(user_id)
            )
        response = requests.get(
            "{}/users/{}/manager".format(self.GRAPH_URL, user_id),
            headers=self._headers(),
        )
        if HTTPStatus.OK == response.status_code:
            result = self._user_from_json(response.json())
        elif HTTPStatus.NOT_FOUND:
            pass
        else:
            message = self._error_message(response)
            raise MicrosoftGraphError(
                "Cannot get the manager for user '{}': {}".format(
                    user_id, message
                )
            )
        return result

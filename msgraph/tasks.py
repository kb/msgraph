# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from django.contrib.auth import get_user_model

from login.models import SYSTEM_GENERATED
from mail.models import Notify
from mail.service import queue_mail_message
from msgraph.service import update_microsoft_graph_users


logger = logging.getLogger(__name__)


def exception_notify_by_email(decscription, e, system_user):
    email_addresses = [n.email for n in Notify.objects.all()]
    if email_addresses:
        queue_mail_message(
            system_user,
            email_addresses,
            f"Error '{decscription}'",
            f"Cannot '{decscription}': {e}",
        )
    else:
        logging.error(
            "Cannot send email notification.  "
            "No email addresses set-up in 'mail.models.Notify'"
        )


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE, max_retries=0)
def microsoft_graph_users():
    """Update 'MicrosoftGraphUser' and 'MicrosoftGraphUserSupervisor'.

    .. warning:: It would be better to call this function,
                 ``update_microsoft_graph_users``, but this would clash with
                 name of the function we are calling.

    """
    count = None
    logger.info("'update_microsoft_graph_users'...")
    user = get_user_model().objects.get(username=SYSTEM_GENERATED)
    try:
        count = update_microsoft_graph_users(user)
    except Exception as e:
        logger.exception(e)
        exception_notify_by_email("update_microsoft_graph_users", e, user)
        raise
    logger.info(
        "'update_microsoft_graph_users' - {} records - complete".format(count)
    )
    return count


def schedule_update_microsoft_graph_users():
    """APScheduler would like to sync user information from Microsoft Graph.

    .. note:: This task will be run on the Windows server.
              It needs access to the Microsoft Graph API.
              The scheduler doesn't have access to the Windows server,
              so we use this funciton to send the message to
              the ``DRAMATIQ_QUEUE_NAME_PIPELINE`` queue.

    https://www.kbsoftware.co.uk/crm/ticket/4751/

    """
    microsoft_graph_users.send()

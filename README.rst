Microsoft Graph
***************

KB Software Documentation:
https://www.kbsoftware.co.uk/docs/app-msgraph.html

The initial requirement is to convert a Microsoft Word file into a PDF for
https://www.kbsoftware.co.uk/crm/ticket/3597/

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-msgraph
  # or
  python3 -m venv venv-msgraph

  source venv-msgraph/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
